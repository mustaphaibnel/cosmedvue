<?php

namespace App\Http\Requests;

use App\Rules\ReCaptcha;
use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'civilite'=>'required|max:30',
            'prenom'=>'required|max:30',
            'nom'=>'required|max:30',
            'email'=>'required|max:60',
            'date_naissance'=>'max:10',
            'telephone'=>'',
            'adresse'=>'',
            'code_postal'=>'',
            'ville'=>'',
            'pays'=>'integer',
            'societe'=>'',
            //'newsletter'=>'bool',
            'destanataire'=>'integer',
            'message'=>'',
            'file_url'=>'',
            //'g-recaptcha-response'=>['required',/*new ReCaptcha()*/]
        ];
    }
    /*
    public function  attributes(){
        return [
            'civilite'=>'',
            'prenom'=>'',
            'nom'=>'',
            'email'=>'',
            'date_naissance'=>'',
            'telephone'=>'',
            'adresse'=>'',
            'code_postal'=>'',
            'ville'=>'',
            'pays_id'=>'',
            'societe'=>'',
            'newsletter'=>'',
            'destanataire_id'=>'',
            'contact_id'=>'',
            'message'=>'',
            'file_url'=>'',

            ];
}*/


}
