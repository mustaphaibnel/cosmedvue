<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Destinataire;
use App\Events\EventMailContact;
use App\Http\Requests\ContactRequest;
use App\Message;
use App\Pays;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        //if search existe in cookies sinon va retournéé tous les message
        $search=Cookie::get('search');
        $message=Message::searchName($search->name)
            ->searchDate($search->date_debut,$search->date_fin)
            ->searchservice($search->service)
            ->searchpays($search->pays)
            ->get()->paginate(25);



        return view('contacts.index',['message'=>$message])->withCookie(cookie()->forever('search', $search));

    }


    public function store(ContactRequest $request) {

        //dd($request);
        $contact= new Contact();

        $contact->civilite=$request->civilite;
        $contact->prenom=$request->prenom;
        $contact->nom=$request->nom;
        $contact->email=$request->email;
        $contact->date_naissance=$request->date_naissance;
        $contact->telephone=$request->telephone;
        $contact->adresse=$request->adresse;
        $contact->code_postal=$request->code_postal;
        $contact->ville=$request->ville;
        $contact->pays_id=$request->pays;
        $contact->societe=$request->societe;
        if ($request->newsletter){
            $contact->newsletter=1;
        }

        //find or create User

        //dd($contact);
        $contact = $this->findOrCreateContact($contact);

        //
        $message=new Message();
        $message->destinataire_id=$request->destinataire;
        $message->message=$request->message;
        if ($request->hasFile('file_url')) {
            $message->file_url=$request->file('file_url')->store('storeCv');
        }else{
            $message->file_url='';
        }

        $message->contact()->associate($contact);
        $message->save();


        $ip=$request->getClientIp();
        $message->ip;
        event(new EventMailContact($message));


        return view('contacts.success');
    }

    public function findOrCreateContact($contact)
    {
        $findcontact = Contact::whereEmail($contact->email)->first();

        if ($findcontact) {
            return $findcontact;
        } else {
            return $contact->save();

        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pays=Pays::all();
        $destinataires=Destinataire::all();
        $data=['pays'=>$pays,'destinataires'=>$destinataires];
        //return view('Contacts.index',['pays' => $pays]);
       // return view('contacts.index',['data'=>$data]);
         return view('contacts.create',['pays'=>$pays,'destinataires'=>$destinataires]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    /**
     * Display the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function show( $contact)
    {
        $contact=Contact::find($contact);
        return view('contacts.show',['contact'=>$contact]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit(Contact $contact)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {

        $search ='';
        $search->name=$request->name;
        $search->date_debut=$request->date_debut;
        $search->date_fin=$request->date_fin;
        $search->service=$request->service;
        $search->pays=$request->pays;

        $message=Message::searchName($search->name)
            ->searchDate($search->date_debut,$search->date_fin)
            ->searchservice($search->service)
            ->searchpays($search->pays)
            ->get()->paginate(25);



        return view('contacts.index',['message'=>$message])->withCookie(cookie()->forever('search', $search));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contact $contact)
    {
        //
    }
}
