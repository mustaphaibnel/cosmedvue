<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Destinataire;
use App\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Laracsv\Export;


class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

//dd(Session::get('search'));
        if(Session::get('search')){
        $messages=$this->getSearch();

        }else{
            $messages=Message::paginate(25);
        }

        $search=$this->oldSearch();

        //
       //dd($_COOKIE['search']);
        //$search= Cookie::get('message');
        //return Response()->json($search);

        $destinataires=Destinataire::orderBy('nom')->get();
        $contacts=Contact::orderBy('nom')->get();
        return view('admin.messages.index',['messages'=>$messages,'contacts'=>$contacts,'destinataires'=>$destinataires,'search'=>$search]);

    }
    public function getSearch()
    {
        $message =Session::get('search.message');
        $datedebut =Session::get('search.dateDebut');
        $datefin =Session::get('search.dateFin');
        $destinataire =Session::get('search.destinataire');
        $contact =Session::get('search.contact');

       return $messages=Message::searchMessage($message)
            ->searchDate($datedebut,$datefin)
            ->searchDestinataire($destinataire)
            ->searchContact($contact)
            ->paginate(23);
    }

    /**
     * @return array
     */
    public function oldSearch()
    {
        $search=[
            'message'=>Session::get('search.message')!=null?Session::get('search.message'):null,
            'dateDebut'=>Session::get('search.dateDebut')!=null?Session::get('search.dateDebut'):null,
            'dateFin'=>Session::get('search.dateFin')!=null?Session::get('search.dateFin'):null,
            'destinataire'=>Session::get('search.destinataire')!=null?Session::get('search.destinataire'):null,
            'contact'=>Session::get('search.contact')!=null?Session::get('search.contact'):null,
        ];

        return collect($search);


    }
    public function search(Request $request)
    {
        Session::put('search', $request->all());

        return Redirect('admin/messages');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteSearch()
    {
        $messages=$this->getSearch();

        foreach ($messages as $message)
        {
            $message->delete();
        }

        return Redirect('admin/messages');
    }

    public function csvSearch()
    {
        $message =Session::get('search.message');
        $datedebut =Session::get('search.dateDebut');
        $datefin =Session::get('search.dateFin');
        $destinataire =Session::get('search.destinataire');
        $contact =Session::get('search.contact');
         $messages=Message::searchMessage($message)
            ->searchDate($datedebut,$datefin)
            ->searchDestinataire($destinataire)
            ->searchContact($contact)
            ->get();
        $csvExporter = new Export();
        $csvExporter->build($messages,['message', 'created_at'])->download('cosmed_messages_'.date("YmdHis").'.csv');

        return Redirect('admin/messages');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function show($message)
    {
        $message=Message::with('contact','destinataire')->findOrfail($message);

        return view('admin.messages.show',['message'=>$message]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function edit(Message $message)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Message $message)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function destroy($message)
    {
        $message=Message::findOrfail($message);
        $message->delete();
        return Redirect('admin/messages');
    }
}
