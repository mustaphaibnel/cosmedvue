<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    public  function contact(){
        return $this->belongsTo(Contact::class);
    }
    public function destinataire(){
        return $this->belongsTo(Destinataire::class);
    }

    public function scopeSearchMessage($query,$name=null)
    {
        if($name==null){
            return $query;
        }
        return $query->where('message','like' ,'%'.$name.'%');
    }

    public function scopeSearchDate($query,$date_debut=null,$date_fin=null)
    {
        if($date_debut==null or $date_fin ==null){
            return $query;
        }
        return $query->whereBetween('created_at', [$date_debut, $date_fin]);
    }

    public function scopeSearchDestinataire($query,$service=null)
    {
        if($service==null){
            return $query;
        }
        return $query->where('destinataire_id', $service);
    }
    public function scopeSearchContact($query,$contact=null)
    {
        if($contact==null){
            return $query;
        }
        return $query->where('contact_id', $contact);
    }
}
