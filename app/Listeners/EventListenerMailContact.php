<?php

namespace App\Listeners;

use App\Events\EventMailContact;
use App\Notifications\ContactMail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EventListenerMailContact implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EventMailContact  $event
     * @return void
     */
    public function handle(EventMailContact $event)
    {
        $contact=$event->message->contact;
        $contact->notify(new ContactMail($event->message));

    }
}
