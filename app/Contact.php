<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use phpDocumentor\Reflection\Types\Null_;


class Contact extends Model
{
    use Notifiable;

    public function messages(){
        return $this->hasMany(Message::class);
    }
    public function pays(){
        return $this->belongsTo(Pays::class);
    }
    public function scopeSearchByColumn($query,$colmun=null,$valeur=null)
    {
        if($colmun==null or $valeur ==null){
            return $query;
        }
        return $query->Where('$colmun', 'like', $valeur);
    }



}
