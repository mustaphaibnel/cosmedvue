<?php

use Faker\Generator as Faker;

$factory->define(App\Destinataire::class, function (Faker $faker) {
    return [
    'nom'=>$faker->name,
    'email'=>$faker->email,
    'fonction'=>$faker->jobTitle
    ];
});
