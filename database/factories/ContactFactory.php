<?php

use Faker\Generator as Faker;

$factory->define(App\Contact::class, function (Faker $faker) {
    return [

    'civilite'=>$faker->randomElement(['monsieur','madame','mzl']),
    'prenom'=>$faker->lastName,
    'nom'=>$faker->firstName,
    'email'=>$faker->email,
    'date_naissance'=>$faker->date(),
    'telephone'=>$faker->phoneNumber,
    'adresse'=>$faker->address,
    'code_postal'=>$faker->postcode,
    'ville'=>$faker->city,
    'pays_id'=>App\Pays::all()->random()->id,
    'societe'=>$faker->company,
    'newsletter'=>$faker->boolean(50)
    ];
});
