<?php

use Faker\Generator as Faker;

$factory->define(App\Message::class, function (Faker $faker) {
    return [

    'destinataire_id'=>App\Destinataire::all()->random()->id,
    'contact_id'=>App\Contact::all()->random()->id,
    'message'=>$faker->text(500),
    'file_url'=>$faker->imageUrl(),
    'created_at'=>$faker->dateTimeThisMonth,
    ];
});
