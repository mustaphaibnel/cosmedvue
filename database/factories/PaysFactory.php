<?php

use Faker\Generator as Faker;

$factory->define(App\Pays::class, function (Faker $faker) {
    return [
        'nom'=>$faker->country,
    ];
});
