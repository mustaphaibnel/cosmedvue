<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('civilite',15);
            $table->string('prenom',30);
            $table->string('nom',30);
            $table->string('email')->unique();
            $table->Date('date_naissance')->nullable();
            $table->string('telephone',50)->nullable();
            $table->text('adresse',150)->nullable();
            $table->text('code_postal',50)->nullable();
            $table->text('ville',50)->nullable();
            $table->integer('pays_id');
            $table->text('societe',150)->nullable();
            $table->boolean('newsletter')->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
