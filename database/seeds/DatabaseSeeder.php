<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\User::class,1)->create();
        factory(\App\Pays::class,10)->create();
        factory(\App\Contact::class,30)->create();
        factory(\App\Destinataire::class,3)->create();
        factory(\App\Message::class,60)->create();
        // $this->call(UsersTableSeeder::class);
    }
}
