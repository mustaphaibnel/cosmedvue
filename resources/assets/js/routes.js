import VueRouter from 'vue-router';
import Form  from './components/FormForm';
import Succes from './components/Succes';
import Contact from './components/Contact';


let routes = [
    {
        path: '/',
        component: Form
    },
    {
        path: '/succes',
        component: Succes
    },
    {
        path: '/contact',
        component: Contact
    }
];


export default new VueRouter({
    routes,

});