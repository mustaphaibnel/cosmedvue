@extends('layouts.app')

@section('content')
<style>
    .form-control {
        width: 200px;
        display: inline-block;
        margin: 5px;
    }
</style>
<div class="container-fluid mt-4">
        <div class="row">
           <div class="col-8 offset-2" >
               <h3>List of Users</h3>
               <div class="form-group">
                       <label>Filter par </label>
                       <select name="payes" id="" class="form-control">
                           <option name="0">Directeur commercial</option>
                           <option name="1">Responsable SAV</option>
                           <option name="3">Responsable ressources humaines </option>
                       </select>
                   <div >
                       <label>Date Debut</label>
                       <input type="date" name="date_debut" class="form-control">
                       <label>Date Fin</label>
                       <input type="date" name="date_fin" class="form-control">
                   </div>
                  <div class="form-group">
                      <label>Mots Cles</label>
                      <input type="text" name="mots_cles" class="form-control">
                  </div>
               </div>
                <hr />
                <hr />
               @if(isset($contacts))
               <div class="card-columns">
                   @foreach($contacts as $contact)
                       <div class="card">
                           <div class="card-body text-center">
                               <p class="card-text"><a href="{{route('admin_contact',$contact->id)}}">{{$contact->nom}} - {{$contact->prenom}}</a></p>
                           </div>
                       </div>
                   @endforeach

                </div>
                   <div>
                       {{ $contacts->links()}}
                   </div>
                @endif
           </div>

        </div>
</div>

@endsection
