@extends('layouts.app')


@section('content')

    <form method="POST" action="{{route('admin.messages.search')}}">
        @csrf
    <div class="row">

        <div class="col-4">
            <div class="form-group">
                <label>message</label>
                <input type="text" class="form-control" name="message" value="{{$search->get('message')}}">
            </div>


            <div class="form-group">
                <label>destinataire</label>
                <select class="form-control" name="destinataire">
                    <option   value> -- Tout -- </option>
                    @foreach($destinataires as $destinataire)
                        <option value="{{$destinataire->id}}" @if($destinataire->id==$search->get('destinataire')) selected @endif>{{$destinataire->nom}}</option>
                    @endforeach
                </select>
            </div>
        </div>


        <div class="col-4">

            <div class="form-group">
                <label>date debut </label>
                <input type="date" class="form-control" name="dateDebut" value="{{$search->get('dateDebut')}}">
            </div>
            <div class="form-group">
                <label>contact</label>
                <select class="form-control" name="contact">
                    <option   value> -- Tout -- </option>
                    @foreach($contacts as $contact)
                    <option value="{{$contact->id}}" @if($contact->id==$search->get('contact')) selected @endif>{{$contact->nom}}</option>
                        @endforeach
                </select>
            </div>

        </div>
        <div class="col-4">

                <div class="form-group">
                    <label>date fin</label>
                    <input type="date" class="form-control" name="dateFin" value="{{$search->get('dateFin')}}">
                </div>

            <div class="form-group">
                <label>Recherchez</label>
                <input type="submit" class="form-control" value="search" class="btn btn-success">
            </div>
        </div>
    </div>
    </form>


    <div class="row">
        @if($messages->count()==0)
            <div class="alert alert-danger col-12">
                <strong>Woops!</strong> aucune Resultat.
            </div>
        @else

            <div class="alert alert-success col-12">
                <strong>{{$messages->total()}} Resultat Trouvé.</strong> vous voulez exporter
                <a href="{{route('admin.messages.deletesearch')}}" class="btn badge-danger"> <i class="fa fa-trash"> delete all</i>
                </a>  <a href="{{route('admin.messages.csvsearch')}}" class="btn badge-success"> <i class="fa fa-file-o">export To Csv</i></a>
            </div>
        @endif

        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th>message</th>
                <th>date</th>
                <th>contact</th>
                <th>destinataire</th>
                <th>show</th>
                <th>delete</th>
            </tr>
            </thead>
            <tbody>

                @foreach($messages as $message)
                    <tr>
                    <td> {{\Illuminate\Support\Str::limit($message->message, 40)}}</td>
                    <td> {{$message->created_at}}</td>
                    <td>{{$message->contact->nom}}</td>
                    <td>{{$message->destinataire->nom}}</td>
                    <td><a href="{{route('admin.messages.show',$message->id)}}"> show</a></td>
                    <td><a href="{{route('admin.messages.destroy',$message->id)}}"> delete</a></td>
                    </tr>
                @endforeach

            </tbody>

        </table>

    </div>

    {{ $messages->links() }}
    @endsection