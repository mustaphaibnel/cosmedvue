@extends('layouts.app')

@section('content')




        <div class="row">
            <div class="col-6">
                <div class="list-group">
                    <a href="#" class="list-group-item list-group-item-action active">
                        Contact
                    </a>
                    <a href="#" class="list-group-item list-group-item-action">nom:{{$message->contact->nom}}</a>
                    <a href="#" class="list-group-item list-group-item-action">nombre de message:{{$message->contact->messages->count()}}</a>
                    <a href="#" class="list-group-item list-group-item-action">prenom:{{$message->contact->prenom}}</a>
                    <a href="#" class="list-group-item list-group-item-action">email:{{$message->contact->email}}</a>
                    <a href="#" class="list-group-item list-group-item-action">ville:{{$message->contact->ville}}</a>
                    <a href="#" class="list-group-item list-group-item-action disabled">civilite:{{$message->contact->civilite}}</a>
                </div>
                
            </div>
            <div class="col-6">
                <div class="list-group">
                    <a href="#" class="list-group-item list-group-item-action active">
                        Destinataire
                    </a>
                    <a href="#" class="list-group-item list-group-item-action">nom:{{$message->destinataire->nom}}</a>
                    <a href="#" class="list-group-item list-group-item-action">email:{{$message->destinataire->email}}</a>
                    <a href="#" class="list-group-item list-group-item-action">nombre de message reçu:{{$message->destinataire->messages->count()}}</a>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="jumbotron">
                <h1>Message</h1>
                <p>{{$message->message}}</p>
            </div>
        </div>

    @endsection