@extends('layouts.app')
@section('content')
    <div class="container mt-5">
        <div class="row">
            <div class="col-10 offset-2">
                <h3>User Detail</h3>
                <div class="card-columns">
                    <div class="card">
                        <div class="card-body text-center">
                            <p class="card-text">
                                <a href="{{route('admin_contact',$contact->id)}}">

                                   {{$contact->nom}}  {{$contact->prenom}}</a></p>
                            <p>{{$contact->email}}</p>
                            <p>{{$contact->civilite}}</p>
                            <p>{{$contact->telephone}}</p>
                            <p>{{$contact->adresse}}</p>
                            <p>{{$contact->ville}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection