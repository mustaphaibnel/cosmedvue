@extends('layouts.app')
@section('javascript')

    <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection
@section('content')

   <div class="container mt-3" id="form">
       <div class="row">
           <div class="col-12">
               <h3>Contactez-nous</h3>
               <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur delectus, dolorem eos excepturi laborum, modi officiis omnis quae,
                   quasi qui quisquam quod repudiandae? Amet illo ipsum, quas quisquam sunt voluptatem.</p>
               <div class="form-area">
       <h3>Informations Personnelles</h3>
           <form action="{{route('contact.store')}}" method="POST"  enctype="multipart/form-data">
               @csrf
               <div class="form-group mb-2 mt-4">
                   <label>Civilite: *</label>
                   <input type="radio" name="civilite" value="M" > M &nbsp;
                   <input type="radio" name="civilite" value="Mlle" > Mlle &nbsp;
                   <input type="radio" name="civilite" value="Mr" > Mr &nbsp;
                   @if(isset($errors))
                       @if ($errors->has('civilite'))
                           <p class="alert alert-danger"> {{ $errors->first('civilite') }}</p>
                       @endif
                   @endif
               </div>
               <div class="form-group">
                   <label>Nom : *</label>
                   <input type="text" name="nom" class="form-control"  value="{{ old('nom') }}" >
                  @if(isset($errors))
                    @if ($errors->has('nom'))
                      <p class="alert alert-danger"> {{ $errors->first('nom') }}</p>
                   @endif
                   @endif
               </div>
               <div class="form-group">
                   <label>Prenom : *</label>
                       <input type="text" name="prenom" class="form-control" value="{{ old('prenom') }}">
                   @if(isset($errors))
                       @if ($errors->has('prenom'))
                           <p class="alert alert-danger"> {{ $errors->first('prenom') }}</p>
                       @endif
                   @endif
               </div>
               <div class="form-group">
                   <label>Date naissance : *</label>
                   <input type="date" name="date_naissance" class="form-control" value="{{ old('date_naissance') }}">
                   @if(isset($errors))
                       @if ($errors->has('prenom'))
                           <p class="alert alert-danger"> {{ $errors->first('prenom') }}</p>
                       @endif
                   @endif
               </div>
               <div class="form-group">
                   <label>Email : *</label>
                   <input type="text" name="email" class="form-control" value="{{ old('email') }}">
                   @if(isset($errors))
                       @if ($errors->has('email'))
                           <p class="alert alert-danger"> {{ $errors->first('email') }}</p>
                       @endif
                   @endif
               </div>
               <div class="form-group">
                   <label>Telephone : *</label>
                   <input type="text" name="telephone" class="form-control" value="{{ old('telephone') }}">
                   @if(isset($errors))
                       @if ($errors->has('tele'))
                           <p class="alert alert-danger"> {{ $errors->first('tele') }}</p>
                       @endif
                   @endif
               </div>
               <div class="form-group">
                   <label>Paye : *</label>
                   <select name="pays" id="" class="form-control">
                       @foreach($pays as $pay)
                       <option value="{{$pay->id}}">{{$pay->nom}}</option>
                       @endforeach
                   </select>
               </div>
               <div class="form-group">
                   <label>Ville : *</label>
                   <input type="text" name="ville" class="form-control" value="{{ old('ville') }}">
               </div>
               <div class="form-group">
                   <label>Code Postal : *</label>
                   <input type="text" name="code_postal" class="form-control" value="{{ old('code_postal') }}">
               </div>
               <div class="form-group">
                   <label>Societe : *</label>
                   <input type="text" name="societe" class="form-control" value="{{ old('societe') }}">
               </div>
               <div class="form-group">
                   <label>Adresse : *</label>
                   <textarea rows="3" cols="22" name="adresse" class="form-control">{{ old('adresse') }}</textarea>
                   @if(isset($errors))
                       @if ($errors->has('adresse'))
                           <p class="alert alert-danger"> {{ $errors->first('adresse') }}</p>
                       @endif
                   @endif
               </div>
       <h3>Message</h3>
               <div class="form-group">
                   <label>Destinataire : *</label>
                   <select name="destinataire" id="destinataire" class="form-control" >
                       @foreach($destinataires as $destinataire)
                           <option value="{{$destinataire->id}}">{{$destinataire->nom}}</option>
                       @endforeach
                   </select>
               </div>
               <div class="form-group" id="file_url">
                   <label>Votre CV : *</label>
                   <input type="file" class="form-control-file" name="file_url"  >
                   @if(isset($errors))
                       @if ($errors->has('uploadedFileName'))
                           <p class="alert alert-danger"> {{ $errors->first('file_url') }}</p>
                       @endif
                   @endif
               </div>
               <div class="form-group">
                   <label>Message : *</label>
                       <textarea rows="10" cols="80" name="message" class="form-control" >{{ old('message') }}</textarea>
                   @if(isset($errors))
                       @if ($errors->has('message'))
                           <p class="alert alert-danger"> {{ $errors->first('message') }}</p>
                       @endif
                   @endif
               </div>
               <div class="form-group">
                   <input type="checkbox" name="newsletter" value="true">
                   <label>Je souhaite recevoir la newsletter.</label>
               </div>
               <div class="g-recaptcha" data-sitekey="6LdxoGYUAAAAAN6PGKCH3HoSk-aHEHwdPcg3l_Wz"></div>
               <div class="form-group">

                       <input type="submit" value="Envoyer" class="btn btn-success">
               </div>
          </form>
               </div>
           </div>
       </div>
   </div>

@endsection

@section('vuejs')
    <script>
        window.onload = function() {
            var input = document.getElementById('file_url');
            input.style.visibility = 'hidden';
            var destinataire = document.getElementById('destinataire');
            destinataire.onchange = function () {
                if (destinataire.options[destinataire.selectedIndex].value =='3') {
                    input.style.visibility = 'visible';
                } else {
                    input.style.visibility = 'hidden';
                }
            }
        }
    </script>
    @endsection