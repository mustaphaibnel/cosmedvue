@extends('layouts.app')

@section('javascript')

    <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection

@section('content')

    <div class="container">
        <div class="row">
            <strong>
                Nous vous remercions pour votre intérêt, votre message a été envoyé.
            </strong>
        </div>
    </div>
@endsection