<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});


//page Contact
Route::get('/contacts/create','ContactController@create')->name('contact.create');
/*
 * save  contact si n'existe pas deja
 * save message
 * dispatch event
 * send email
*/
Route::post('/contacts','ContactController@store')->name('contact.store');



Route::middleware(['auth', 'isAdmin'])->group(function () {


    //lister tous les message avec contact a partir cookies ou default
    Route::get('admin/messages','MessageController@index')->name('admin.messages.index');
    Route::get('admin/deletesearch','MessageController@deleteSearch')->name('admin.messages.deletesearch');
    Route::get('admin/csvsearch','MessageController@csvSearch')->name('admin.messages.csvsearch');
    //lister tous les message avec filter poster & update cookies
    Route::post('admin/messages/search','MessageController@search')->name('admin.messages.search');
    //affichier contact avec ses message
    Route::get('admin/messages/{id}','MessageController@show')->name('admin.messages.show');
    Route::get('admin/messages/{id}/delete','MessageController@destroy')->name('admin.messages.destroy');


});



Route::get('/email',function (){
    $message=\App\Message::first();
    event(new \App\Events\EventMailContact($message));
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/contact', function () {
    return view('ContactUs');
})->name('contact.us');

Route::get('/admin/contact', function () {
    return view('ContactAdmin');
})->name('contact.admin');
